import React from "react";
import Game from "./components/game/Game.js";
import "./styles.css";
function App() {
  return (
    <div id="game">
      <Game />
    </div>
  );
}

export default App;
