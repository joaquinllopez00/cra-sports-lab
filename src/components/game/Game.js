import React from "react";
import Teams from "../teams/Teams";
import TeamOnePic from "../../assets/batmanlogo.jpg";
import TeamTwoPic from "../../assets/supermanlogo.png";
function Game(props) {
  const venue = props.venue;
  return (
    <div>
      <div id="venueHeader">Welcome to {venue}</div>
      <div id="container">
        <div id="teamOne">
          <Teams team="Home" teamName="Batman" teamImage={TeamOnePic} />
        </div>
        <h1 id="versus">VS</h1>
        <div id="teamTwo">
          <Teams team="Visiting" teamName="Superman" teamImage={TeamTwoPic} />
        </div>
      </div>
    </div>
  );
}

export default Game;
